#ifndef _CHUNKMANAGEMENT_H_
#define _CHUNKMANAGEMENT_H_

#include <iostream>
#include <string>
#include <list>

#include "Chunk.h"
#include "Voxel.h"


using namespace std;

class CChunkManagement // Class that allows to manage chunks
{
	private :
		std::list<CChunk> ChunkLoadList;
		std::list<CChunk> ChunkRenderList;
		std::list<CChunk> ChunkUnloadList;
		std::list<CChunk> ChunkVisibilityList;
		std::list<CChunk> ChunkSetupList;
		std::list<CVoxel> LiquidVoxelRenderList;


};
#endif