#ifndef _VOXEL_H_
#define _VOXEL_H_

#include <iostream>
#include <string>

using namespace std;
enum VoxelType
{
	VoxelType_Default = 0,

	VoxelType_Stone,
	VoxelType_Wood,

};
class CVoxel // Class that allows to deal with voxels
{

	// Attributes declaration
private:
	bool a_active;

	VoxelType a_voxelType;

	//Methods declaration
public :
	CVoxel();
	~CVoxel();

	bool IsActive();
	void SetActive(bool active);

};
#endif