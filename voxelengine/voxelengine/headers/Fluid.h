#ifndef _FLUID_H_
#define _FLUID_H_

#include <iostream>
#include <string>
#include <vector_types.h>

using namespace std;
enum FluidType
{
	FluidType_Default = 0,

	FluidType_Water,
	FluidType_Lava,

};
class CFluid // Class that allows to deal with voxels
{

	// Attributes declaration
private:
	FluidType a_fluidType;
	float4 a_density;
	float3 a_pressure;
	float3 a_velocity;
	int a_amount;
	bool a_source;
	float a_temperature;

	//Methods declaration
public:
	CFluid();
	~CFluid();



};
#endif