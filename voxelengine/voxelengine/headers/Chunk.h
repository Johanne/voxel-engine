#ifndef _CHUNK_H_
#define _CHUNK_H_
#include <GL/glew.h>

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>


// Includes
#include <math.h>
#include "Shader.h"
#include <iostream>
#include <string>
#include "Voxel.h"
#include "Texture.h"
#include "Caisse.h"

//Includes libnoise
#include <noise/noise.h>

using namespace noise;

using namespace std;
// Macro utile au VBO

#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif
class CChunk // Class that allows to deal with chunks
{
public:
	static const int CHUNK_SIZE = 16;
	// Attributes declaration
private:
	int wav=0;
	CVoxel*** a_voxels;
	Texture m_texture[2];
	Shader m_shader;
	float m_vertices[108];
	float m_couleurs[108];
	Caisse caisse;
	int chunkX;
	int chunkY;
	int a_taille;

	GLuint m_vboID;
	int m_tailleVerticesBytes;
	int m_tailleCouleursBytes;
	//Methods declaration
public:
	CChunk(int x, int y, float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture);
	~CChunk();

	void CreateMesh();

	void CreateCube();

	void Update();

	void Render();
	
	void charger();
	void afficher(glm::mat4 &projection, glm::mat4 &modelview);
	void updateVBO(void *donnees, int tailleBytes, int decalage);
	float Get2DPerlinNoiseValue(float x, float y, float res);
	void GenChunk();
	void wave();
	void init();
};
#endif