#include "Voxel.h"

CVoxel::CVoxel() 
{
	a_voxelType = VoxelType_Default;
	a_active = false;
}

CVoxel::~CVoxel()
{

}
void CVoxel::SetActive(bool b)
{
	a_active = b;
}
bool CVoxel::IsActive()
{
	return a_active;
}
